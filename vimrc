"" Vim configuration

"" Credits:
""   Gary Bernhardt  <destroyallsoftware.com>
""   Drew Neil  <vimcasts.org>
""   Tim Pope  <tbaggery.com>
""   Janus  <github.com/carlhuda/janus>
""   Mislav <mislav.uniqpath.com/2011/12/vim-revisited>
""   Andrea Pavoni <https://github.com/apeacox/vim_starter_kit>

set nocompatible                  " use Vim, not Vi
filetype off                      " automatically detect file types. Also equired for vundle.

" START Vundle setup
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
Bundle 'gmarik/vundle'

" My Bundles here:

" Utils

" The ultimate vim statusline utility
Bundle 'Lokaltog/vim-powerline'
let g:Powerline_symbols = 'fancy'

" Vim plugin for the Perl module / CLI script 'ack'
Bundle 'mileszs/ack.vim'

" Vim plugin that displays tags in a window, ordered by class etc.
Bundle 'majutsushi/tagbar'

" Vim plugin to list, select and switch between buffers.
Bundle 'jeetsukumaran/vim-buffergator'
let g:buffergator_viewport_split_policy = 'B'

" unimpaired.vim: pairs of handy bracket mappings
Bundle 'tpope/vim-unimpaired'

" hax0r vim script to give you a tree explorer
Bundle 'scrooloose/nerdtree'          

" Vim plugin for intensely orgasmic commenting
Bundle 'scrooloose/nerdcommenter'

" Perform all your vim insert mode completions with Tab
Bundle 'ervandew/supertab'        

" surround.vim: quoting/parenthesizing made simple
Bundle 'tpope/vim-surround'       

" extended % matching for HTML, LaTeX, and many other languages
Bundle 'tsaleh/vim-matchit'       

" buffer/file/command/tag/etc explorer with fuzzy matching. L9 is a requirements
Bundle 'L9'
Bundle 'FuzzyFinder'

" Vim Git
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-git'

" Zoom in/out of windows (toggle between one window and multi-window)
Bundle 'vim-scripts/ZoomWin'

" Vim script for text filtering and alignment
Bundle 'godlygeek/tabular'

" A vim plugin for running your Ruby tests
Bundle 'skalnik/vim-vroom'
let g:vroom_use_vimux = 1
let g:vroom_map_keys = 0

" vim plugin to interact with tmux
Bundle 'benmills/vimux'

" Syntaxes
Bundle 'tpope/vim-haml'
Bundle 'pangloss/vim-javascript'
Bundle 'tpope/vim-cucumber'
Bundle 'tpope/vim-rails'
Bundle 'skwp/vim-rspec'
let g:RspecKeymap = 0

Bundle 'tpope/vim-markdown'
Bundle 'kchmck/vim-coffee-script'
Bundle 'othree/html5.vim'
Bundle 'vim-ruby/vim-ruby'

" Snipmate 
Bundle "MarcWeber/vim-addon-mw-utils"
Bundle "tomtom/tlib_vim"
Bundle "honza/snipmate-snippets"
Bundle "garbas/vim-snipmate"

" Themes
Bundle 'jgdavey/vim-railscasts'
Bundle 'nanotech/jellybeans.vim'
Bundle 'altercation/vim-colors-solarized'
" END Vundle setup

" Theme
set t_Co=256
set background=dark
color solarized

" Editing
filetype indent on                " automatically do language-dependent indenting. Also equired for vundle.
filetype plugin on                " loading the plugin files for specific file types. Also equired for vundle.
syntax on                         " enable syntax highlighting
set encoding=utf-8                " sets the character encoding used inside Vim
set fenc=utf-8                    " sets the character encoding for the file of this buffer
set number                        " line numbers on
set ruler                         " show the cursor position all the time
set visualbell                    " no beeping
set backspace=indent,eol,start    " allow backspacing over everything in insert mode
set autoindent                    " always set autoindenting on
let mapleader=","                 " change mapleader key

" Command line
set history=50                    " keep 50 lines of command line history
set wildmenu                      " Enhanced command line completion.

" Use space instead of tabs
set smartindent
set tabstop=2
set shiftwidth=2
set expandtab

" Searching
set hlsearch                      " highlight matches
set incsearch                     " incremental searching
set ignorecase                    " searches are case insensitive...
set smartcase                     " ... unless they contain at least one capital letter

" Tab completion
set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc

" Status bar
set laststatus=2
set showcmd                       " Display incomplete commands.
set showmode                      " Display the mode you're in.
set hidden                        " Handle multiple buffers better.

" Backups and swap files
set backupdir=~/.vim/backup
set directory=~/.vim/tmp
set nobackup                      " Don't make a backup before overwriting a file.
set nowritebackup
set noswapfile                    " http://robots.thoughtbot.com/post/18739402579/global-gitignore#comment-458413287

" NERDTree
let NERDTreeIgnore=['\.rbc$', '\~$']
map <F2> :NERDTreeToggle<CR>

" Tagbar
let g:tabgbar_ctags_bin="/usr/local/bin/ctags"
nmap <F3> :TagbarToggle<CR>
let g:tagbar_type_ruby = {
  \ 'kinds' : [
    \ 'm:modules',
    \ 'c:classes',
    \ 'd:describes',
    \ 'C:contexts',
    \ 'f:methods',
    \ 'F:singleton methods'
  \ ]
\ }

" Ack
map <leader>a :Ack 

" Write on the fly
map <leader>w :write<CR>
map <leader>q :quit<CR>

" disable arrow keys, use hjkl
noremap  <Up> ""
noremap  <Down> ""
noremap  <Left> ""
noremap  <Right> ""

" navigate split windows
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

" FuzzyFinder (was CommandT)
map <Leader>f :FufCoverageFile<CR>

" ZoomWin
map <Leader><Leader> :ZoomWin<CR>

" Vroom
map <leader>vr :VroomRunTestFile<CR>
map <leader>vR :VroomRunNearestTest<CR>

" vim-spec
map <leader>r ::RunSpec<CR>
map <leader>R :RunSpecLine<CR>

" CTags
set tags=./.tags,.tags
map <f5> :!/usr/local/bin/ctags -f .tags *<CR><CR>
map <f6> :!bundle list --paths=true \| xargs /usr/local/bin/ctags -f .tags *<CR><CR>
